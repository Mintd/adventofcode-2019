package main

import (
	"fmt"
	"gitlab.com/Mintd/AdventOfCode-2019/readintcode"
	"os"
	"sync"
)

func compute(initalState []int, noun int, verb int, wg *sync.WaitGroup) {
	defer wg.Done()

	// copy inital state of the Intcode machine
	codes := make([]int, len(initalState))
	copy(codes, initalState)

	// replace values with chosen noun and verb
	codes[1], codes[2] = noun, verb

	for pc := 0; pc < len(codes); pc += 4 {
		if codes[pc] == 99 {
			break
		} else {
			first := codes[codes[pc+1]]
			second := codes[codes[pc+2]]
			dest := codes[pc+3]

			// add instruction
			if codes[pc] == 1 {
				codes[dest] = first + second
			} else if codes[pc] == 2 {
				codes[dest] = first * second
			} else {
				panic("Invalid instruction")
			}
		}
	}

	// output if desired result is found
	if codes[0] == 19690720 {
		fmt.Printf("noun: %d, verb: %d", noun, verb)
		os.Exit(0)
	}
}

func bruteForce(initialState []int) {
	var wg sync.WaitGroup
	for noun := 0; noun < 100; noun++ {
		for verb := 0; verb < 100; verb++ {
			wg.Add(1)
			go compute(initialState, noun, verb, &wg)
		}
	}

	// wait for all goroutines to finish
	wg.Wait()
}

func main() {
	initalState, err := readintcode.ReadIntcode("input")
	if err != nil {
		panic(err)
	}

	// start spinning up goroutines
	bruteForce(initalState)
}
