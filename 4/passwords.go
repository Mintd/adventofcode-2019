package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
)

func verify(number int) bool {
	// convert number to string for digit by digit comparison
	byteNum := []byte(strconv.Itoa(number))

	// slice of letters that have already been matched
	found := make(map[byte]int)

	// check length
	if len(byteNum) != 6 {
		return false
	}

	// check that the number does not decrease
	// ie. that it is sorted
	if !sort.SliceIsSorted(byteNum, func(x, y int) bool { return byteNum[x] < byteNum[y] }) {
		return false
	}

	// check if there is a double digit
	for index := 0; index < len(byteNum); index++ {
		found[byteNum[index]]++
	}

	for _, v := range found {
		if v == 2 {
			return true
		}
	}

	return false
}

func main() {
	low, errL := strconv.Atoi(os.Args[1])
	high, errH := strconv.Atoi(os.Args[2])
	passCount := 0

	if errL != nil || errH != nil {
		panic("inputs are not integers")
	}

	for ; low <= high; low++ {
		if verify(low) {
			passCount++
		}
	}

	fmt.Printf("Possible passwords: %d", passCount)
}
