package readintcode

import (
	"bufio"
	"errors"
	"os"
	"strconv"
)

// ReadIntcode : read Intcode file and output slice of instructions
func ReadIntcode(inFile string) ([]int, error) {
	codes := make([]int, 0, 20)

	file, err := os.Open("input")
	if err != nil {
		return codes, errors.New("Can't open file")
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	// split at comma
	onComma := func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		for i := 0; i < len(data); i++ {
			if data[i] == ',' {
				return i + 1, data[:i], nil
			}
		}

		if !atEOF {
			return 0, nil, nil
		}
		// There is one final token to be delivered, which may be the empty string.
		// Returning bufio.ErrFinalToken here tells Scan there are no more tokens after this
		// but does not trigger an error to be returned from Scan itself.
		return 0, data, bufio.ErrFinalToken
	}

	scanner.Split(onComma)

	// start reading codes one at a time
	for scanner.Scan() {
		code, _ := strconv.Atoi(scanner.Text())
		codes = append(codes, code)
	}

	return codes, nil
}
