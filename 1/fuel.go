package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	fuel := 0
	file, _ := os.Open("input")
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		mass, _ := strconv.Atoi(scanner.Text())

		lastCalc := (mass / 3) - 2

		for lastCalc > 0 {
			fuel += lastCalc
			lastCalc = (lastCalc / 3) - 2
		}
	}

	fmt.Println(fuel)
}
