package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"

	geo "github.com/paulmach/go.geo"
)

func main() {
	file, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	scanner := bufio.NewScanner(file)

	wires := make([]*geo.Path, 2)

	// build wires
	for i := 0; i < cap(wires); i++ {
		wires[i] = geo.NewPath().Push(geo.NewPoint(0, 0))
		scanner.Scan()
		for _, locs := range strings.Split(scanner.Text(), ",") {
			lastPoint := wires[i].GetAt(wires[i].Length() - 1)
			distance, _ := strconv.ParseFloat(string(locs[1:]), 64)

			// add to path
			switch direct := string(locs[0]); direct {
			case "U":
				wires[i].Push(geo.NewPoint(lastPoint.X(), lastPoint.Y()+distance))
			case "D":
				wires[i].Push(geo.NewPoint(lastPoint.X(), lastPoint.Y()-distance))
			case "L":
				wires[i].Push(geo.NewPoint(lastPoint.X()-distance, lastPoint.Y()))
			case "R":
				wires[i].Push(geo.NewPoint(lastPoint.X()+distance, lastPoint.Y()))
			default:
				panic("unrecognized direction")
			}
		}
	}

	// find intersections
	points, _ := wires[0].Intersection(wires[1])

	// calculate taxicab distance from origin
	result := math.MaxFloat64
	for _, point := range points {
		// find the number of steps to reach intersection for each wire
		stepsFirst := wires[0].Measure(point)
		stepsSecond := wires[1].Measure(point)

		steps := stepsFirst + stepsSecond

		if distance := math.Abs(point.X()) + math.Abs(point.Y()); steps > 0 && distance > 0 && steps < result {
			result = steps
		}
	}

	fmt.Println(result)
}
